﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oxmes.Core.ServiceUtilities.Models
{
    public class Service
    {
        public virtual Guid UUID { get; set; }

        public virtual string Name { get; set; }

        public virtual string DisplayName { get; set; }

        public virtual int ServiceType { get; set; }

        public virtual string ServiceAddress { get; set; }

        public virtual bool IsActive { get; set; }

        public override string ToString()
        {
            return $"<UUID:{UUID}, Name:{Name}, DisplayName:{DisplayName}, ServiceType:{ServiceType}, ServiceAddress:{ServiceAddress}>";
        }
    }
}
