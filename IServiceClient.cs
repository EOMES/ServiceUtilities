﻿using System;
using System.Threading.Tasks;

namespace Oxmes.Core.ServiceUtilities
{
    public interface IServiceClient
    {
        string ServiceName { get; set; }
        string ServiceDisplayName { get; set; }
        int ServiceType { get; set; }

        string GatewayHost { get; set; }
        ushort? GatewayPort { get; set; }
        bool UseHttps { get; set; }
        string ApiKey { get; set; }

        Guid ServiceUuid { get; }
        Guid GatewayUuid { get; }

        Task ConnectAsync();

    }
}
