﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Oxmes.Core.ServiceUtilities.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Oxmes.Core.ServiceUtilities
{
    public class ServiceClient : IServiceClient
    {
        public string ServiceName { get; set; }
        public string ServiceDisplayName { get; set; }
        public int ServiceType { get; set; }

        public string GatewayHost { get; set; }
        public ushort? GatewayPort { get; set; }
        public bool UseHttps { get; set; } = false;
        public string ApiKey { get; set; }

        public Guid ServiceUuid { get; protected set; }
        public Guid GatewayUuid { get; protected set; }

        protected string Protocol => UseHttps ? "https" : "http";

        protected readonly ILogger Logger;
        protected readonly IConfiguration Configuration;

        public ServiceClient(ILogger<ServiceClient> logger, IConfiguration configuration)
        {
            Logger = logger;
            Configuration = configuration;
        }

        public async Task ConnectAsync()
        {
            var wait = 0;
            bool success;

            do
            {
                if (wait > 0) Logger.LogInformation($"Waiting {wait / 1000} seconds to retry connecting.");
                Thread.Sleep(wait);
                success = await TryConnectAsync();
                wait = wait > 0 ? Math.Min(wait * 2, 60_000) : 1_000;
            } while (!success);
        }

        public async Task<bool> TryConnectAsync()
        {
            if (GatewayHost == null) throw new InvalidOperationException($"{nameof(GatewayHost)} not set;");
            if (GatewayPort == null) throw new InvalidOperationException($"{nameof(GatewayPort)} not set;");
            if (ApiKey == null) throw new InvalidOperationException($"{nameof(ApiKey)} not set;");
            if (ServiceName == null) throw new InvalidOperationException($"{nameof(ServiceName)} not set;");
            if (ServiceType == 0) throw new InvalidOperationException($"{nameof(ServiceType)} not set;");

            // Build service address

            var remoteScheme = Configuration["remoteEndpoint:scheme"];
            var remoteAddress = Configuration["remoteEndpoint:address"];
            var remotePort = Configuration["remoteEndpoint:port"];
            string serviceAddress;

            if (!string.IsNullOrEmpty(remoteScheme) && !string.IsNullOrEmpty(remoteAddress) && !string.IsNullOrEmpty(remotePort))
            {
                serviceAddress = $"{remoteScheme}://{remoteAddress}:{remotePort}";
            }
            else
            {
                serviceAddress = Configuration["urls"].Split(';')[0];
            }

            var service = new Service()
            {
                Name = ServiceName,
                DisplayName = ServiceDisplayName,
                ServiceType = ServiceType,
                ServiceAddress = serviceAddress,
            };
            var serviceJson = JsonConvert.SerializeObject(service);
            var contentData = new StringContent(serviceJson, Encoding.UTF8, "application/json");
            var http = new HttpClient { BaseAddress = new Uri($"{Protocol}://{GatewayHost}:{GatewayPort}/api/Service/") };

            Logger.LogDebug($"Service: {service.ToString()}");

            http.DefaultRequestHeaders.Add("Authorization", $"Bearer {ApiKey}");

            try
            {
                var response = http.PostAsync("", contentData).Result;
                if (response.IsSuccessStatusCode)
                {
                    var responseService = JsonConvert.DeserializeObject<Service>(await response.Content.ReadAsStringAsync());
                    ServiceUuid = responseService.UUID;

                    // Retrieve Gateway Guid
                    using (var res = await http.GetAsync($"type/100"))
                    {
                        if (res.IsSuccessStatusCode)
                        {
                            if (res.StatusCode != System.Net.HttpStatusCode.NoContent)
                            {
                                var gateway = JsonConvert.DeserializeObject<Service>(await res.Content.ReadAsStringAsync());
                                GatewayUuid = gateway.UUID;
                            }
                        }
                    }

                    return true;
                }
            }
            catch (AggregateException ex) when (ex.InnerException.GetType() == typeof(HttpRequestException))
            {

            }
            return false;
        }
    }
}
